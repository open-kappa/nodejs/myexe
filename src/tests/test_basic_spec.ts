/* eslint-disable max-classes-per-file */
import {
    Cli,
    Config,
    ICommand,
    Main
} from "../myexe/index";
import {MyLogger} from "@open-kappa/mylog";
import {MyTest} from "@open-kappa/mytest";


class ThisCommand
implements ICommand
{
    private readonly main: Main<any>;

    constructor(main: Main<any>)
    {
        this.main = main;
    }

    run(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }
}

class ThisTest
    extends MyTest
{
    constructor()
    {
        super("basic test");
    }

    registerTests(): void
    {
        const self = this;
        self.registerPromiseTest("instantiation", self._testInstantation);
    }

    _testInstantation(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const main = new Main(MyLogger, Cli, Config, ThisCommand, null);
        main.cli.addVerbose();
        main.cli.addConfig([], false, true);
        return main.run();
    }
}

const thisTest = new ThisTest();
thisTest.run();
