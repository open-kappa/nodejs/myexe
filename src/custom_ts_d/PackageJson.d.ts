/**
 * @brief The person type for package.json
 */
export declare interface PackageJsonPerson
{
    email?: string;
    name: string;
    url?: string;
}


/**
 * @brief The Package.json type definition.
 */
export declare interface PackageJson
{
    author?: PackageJsonPerson|string;
    bin?: {[name: string]: string}|string;
    browser?: boolean;
    bugs?: {
        email?: string;
        url?: string;
    };
    bundleDependencies?: {
        [name: string]: string;
    };
    config?: {
        [name: string]: string;
    };
    contributors?: Array<PackageJsonPerson|string>;
    cpu: Array<string>;
    devDependencies?: {
        [name: string]: string;
    };
    dependencies?: {
        [name: string]: string;
    };
    description?: string;
    directories?: {
        bin?: string;
        doc?: string;
        example?: string;
        lib?: string;
        man?: string;
        test?: string;
    };
    engines?: {
        [name: string]: string;
    };
    files?: Array<string>;
    homepage?: string;
    keywords?: Array<string>;
    main: string;
    man?: string;
    name: string;
    optionalDependencies?: {
        [name: string]: string;
    };
    os: Array<string>;
    license: string;
    peerDependencies?: {
        [name: string]: string;
    };
    private?: boolean;
    publishConfig?: {
        access?: string;
        registry?: string;
        tag?: string;
        // many others...
    };
    repository?: {
        directory?: string;
        type: string;
        url: string;
    }
    scripts?: {
        [name: string]: string;
    };
    version: string;
}
