/**
 * @brief The type for an option.
 */
export interface CliOption
{

    /**
     * @brief The short form of a flag. E.g. -h
     */
    shortFlag: string;

    /**
     * @brief The short form of a flag. E.g. -h
     */
    longFlag: string;

    /**
     * The flag descriotion.
     */
    description: string;

    /**
     * Optional default value.
     */
    defaultValues?: Array<string>

    /**
     * Whether the option is mandatory. Default is false.
     */
    mandatory?: boolean;

    /**
     * @brief Whether it can be specified multiple times.
     */
    isMultiple?: boolean;

    /**
     * @brief Whether it has a parameter.
     */
    isFlag?: boolean;

    /**
     * @brief Allowed values.
     */
    allowedValues?: Array<string>;
}
