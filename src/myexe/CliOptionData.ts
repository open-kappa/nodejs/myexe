import {CliOption} from "./index";


/**
 * @brief Support class for CLI parsing.
 */
export class CliOptionData
{
    /**
     * @brief The wrapped option.
     * @private
     */
    private readonly cliOption: CliOption;

    /**
     * @brief The parsed values.
     * @private
     */
    private readonly values: Array<string>;

    /**
     * @brief The number of option occurrences.
     * @private
     */
    private count: number;

    /**
     * @brief Constructor.
     * @param {CliOption} cliOption The option to wrap.
     */
    constructor(cliOption: CliOption)
    {
        this.cliOption = cliOption;
        this.values = [];
        this.count = 0;
    }

    /**
     * @brief Get the flag name.
     * @return {string} The name.
     */
    getName(): string
    {
        const self = this;
        const sf = self.cliOption.shortFlag;
        const lf = self.cliOption.longFlag;
        const hasSep = sf !== "" && lf !== "";
        return sf + (hasSep ? "/" : "") + lf;
    }


    /**
     * @brief Get the flag mandatory.
     * @return {boolean} The check result.
     */
    isMandatory(): boolean
    {
        return this.cliOption.mandatory === true;
    }


    /**
     * @brief Get the flag count.
     * @return {number} The flag count.
     */
    getCount(): number
    {
        return this.count;
    }

    /**
     * @brief Get the parsed values.
     * @return {Array<string>} The parsed values.
     */
    getValues(): Array<string>
    {
        const self = this;
        if (self.count === 0)
        {
            if (typeof self.cliOption.defaultValues !== "undefined")
            {
                return self.cliOption.defaultValues;
            }
            return [];
        }
        return self.values;
    }

    /**
     * @brief Get the value of a boolean flag.
     * @return {boolean} The flag value.
     */
    getFlagValue(): boolean
    {
        const self = this;
        if (self.count === 0)
        {
            if (typeof self.cliOption.defaultValues !== "undefined")
            {
                return self.cliOption.defaultValues[0] === "true";
            }
            return false;
        }
        return true;
    }

    /**
     * @brief Parse a value
     * @param {Array<string>} argv The argv vector.
     * @param {number} index The current parsing index.
     * @return {number|Error} The new index.
     */
    parse(argv: Array<string>, index: number): number|Error
    {
        const self = this;
        if (!self.cliOption.isMultiple && self.count > 0)
        {
            return new Error(
                "Option " + self.cliOption.shortFlag
                + " cannot be specified multiple times"
            );
        }

        if (self.cliOption.isFlag)
        {
            self.count += 1;
            return index;
        }

        const ret = index + 1;
        if (ret >= argv.length)
        {
            return new Error(
                "Option " + self.cliOption.shortFlag
                + " requires a parameter"
            );
        }
        const param = argv[ret];
        if (self.cliOption.allowedValues)
        {
            const ind = self.cliOption.allowedValues.indexOf(param);
            if (ind === -1)
            {
                return new Error(
                    "Option " + self.cliOption.shortFlag
                    + ", invalid value: " + param
                );
            }
        }
        self.values.push(param);
        self.count += 1;
        return ret;
    }

    toString(): string
    {
        const self = this;
        const name = self.getName();
        const desc = self.cliOption.description;
        const isMandatory = self.cliOption.mandatory === true;
        const isFlag = self.cliOption.isFlag === true;
        const isMultiple = self.cliOption.isMultiple === true;
        let allowedValues = false;
        let values = "";
        if (typeof self.cliOption.allowedValues !== "undefined")
        {
            allowedValues = true;
            values = " <" + self.cliOption.allowedValues.join("|") + ">";
        }

        return (isMandatory ? "" : "[")
            + name
            // eslint-disable-next-line no-nested-ternary, no-extra-parens
            + (isFlag ? "" : (allowedValues ? values : " <param>"))
            + (isMultiple ? "..." : "")
            + (isMandatory ? "" : "]")
            + ": " + desc;
    }
}
