import {
    CliOption,
    CliOptionData,
    Main
} from "./index";
import {
    MyConsoleLogger,
    MyLogLevel
} from "@open-kappa/mylog";
import path from "path";


/**
 * @brief The class implementing the cli.
 * It automatically adds the version flag (-V/--version) and the help
 * (-h/--help).
 * It can be extended for customization.
 * @typeParam JsonConfig The JSON configuration type.
 */
class Cli<JsonConfig>
{
    /**
     * @brief The reference to main.
     * @protected
     */
    protected readonly main: Main<JsonConfig>;

    /**
     * @brief The flags.
     * @protected
     */
    protected flags: Array<CliOptionData>;

    /**
     * @brief The flags map.
     * @protected
     */
    protected flagsMap: Map<string, CliOptionData>;

    /**
     * @brief The list of unparsed args.
     * @protected
     */
    protected unparsedArgs: Array<string>;

    /**
     * @brief Whether has the version flag.
     * @protected
     */
    protected hasVersion: boolean;

    /**
     * @brief Whether has the help flag.
     * @protected
     */
    protected hasHelp: boolean;

    /**
     * @brief Whether has the verbose flag.
     * @protected
     */
    protected hasVerbose: boolean;

    /**
     * @brief Whether has the config flag.
     * @protected
     */
    protected hasConfig: boolean;

    /**
     * @brief Whether has the output flag.
     * @protected
     */
    protected hasOutput: boolean;

    /**
     * @brief Whether has unparsed args.
     * Zero means no args, -1 means any length.
     * @protected
     */
    protected unparsedArgsNumber: number;

    /**
     * @brief Optional message to print as help for unparsed args.
     * @protected
     */
    protected unparsedArgsMessage: string;

    /**
     * @brief Additional messages to print as help.
     * @protected
     */
    protected extraHelp: Array<string>;

    /**
     * @brief Constructor.
     * @param {Main<JsonConfig>} main The reference to main.
     */
    constructor(main: Main<JsonConfig>)
    {
        this.main = main;
        this.flags = [];
        this.flagsMap = new Map();
        this.unparsedArgs = [];
        this.hasVersion = false;
        this.hasHelp = false;
        this.hasVerbose = false;
        this.hasConfig = false;
        this.hasOutput = false;
        this.unparsedArgsNumber = 0;
        this.unparsedArgsMessage = "";
        this.extraHelp = [];
    }

    /**
     * @brief Generic method to add an option.
     * @param {CliOption} opt The option.
     * @return {Promise<void>} A completion promise.
     */
    addOption(opt: CliOption): Promise<void>
    {
        const self = this;
        if (self.flagsMap.has(opt.shortFlag))
        {
            return Promise.reject(
                new Error("Flag already used: " + opt.shortFlag)
            );
        }
        else if (self.flagsMap.has(opt.longFlag))
        {
            return Promise.reject(
                new Error("Flag already used: " + opt.longFlag)
            );
        }
        const newOpt = new CliOptionData(opt);
        self.flags.push(newOpt);
        if (opt.shortFlag !== "") self.flagsMap.set(opt.shortFlag, newOpt);
        if (opt.longFlag !== "") self.flagsMap.set(opt.longFlag, newOpt);
        return Promise.resolve();
    }

    /**
     * @brief Add the verbosity flag (-v/--verbose).
     * @return {Promise<void>} A completion promise.
     */
    addVerbose(): Promise<void>
    {
        const self = this;
        self.hasVerbose = true;
        const opt: CliOption = {
            "description": "increase the verbosity",
            "isFlag": true,
            "isMultiple": true,
            "longFlag": "--verbose",
            "shortFlag": "-v"
        };
        return self.addOption(opt);
    }

    /**
     * @brief Add the config flag (-c/--config).
     * @param {Array<string>} defaultValues The default values.
     * @param {boolean} required True if required.
     * @param {boolean} single Whether it can be specified only once.
     * @return {Promise<void>} A completion promise.
     */
    addConfig(
        defaultValues: Array<string>,
        required: boolean,
        single: boolean
    ): Promise<void>
    {
        const self = this;
        self.hasConfig = true;
        const opt: CliOption = {
            "defaultValues": defaultValues,
            "description": "pass a configuration path",
            "isMultiple": !single,
            "longFlag": "--config",
            "mandatory": required,
            "shortFlag": "-c"
        };
        return self.addOption(opt);
    }

    /**
     * @brief Add the output flag (-o/--output).
     * @param {boolean} required True if required.
     * @return {Promise<void>} A completion promise.
     */
    addOutput(required: boolean): Promise<void>
    {
        const self = this;
        self.hasOutput = true;
        const opt: CliOption = {
            "description": "specify the output path",
            "longFlag": "--output",
            "mandatory": required,
            "shortFlag": "-o"
        };
        return self.addOption(opt);
    }

    /**
     * @brief Add the version (-V/--version).
     * @protected
     * @return {Promise<void>} A completion promise.
     */
    protected addVersion(): Promise<void>
    {
        const self = this;
        self.hasVersion = true;
        const opt: CliOption = {
            "description": "print the version",
            "isFlag": true,
            "longFlag": "--version",
            "shortFlag": "-V"
        };
        return self.addOption(opt);
    }

    /**
     * @brief Add the help (-h/--help).
     * @protected
     * @return {Promise<void>} A completion promise.
     */
    protected addHelp(): Promise<void>
    {
        const self = this;
        self.hasHelp = true;
        const opt: CliOption = {
            "description": "print the help",
            "isFlag": true,
            "longFlag": "--help",
            "shortFlag": "-h"
        };
        return self.addOption(opt);
    }

    /**
     * @brief Add the support for unparsed args.
     * Parameter args can be zero (default) to disable, or negative to enable
     * any length, or any positive value to allow that exact ammount.
     * @param {number} args The number of expected args.
     * @param {string} msg The help message to print for the args.
     */
    addUnparsedArgs(args: number, msg: string): void
    {
        this.unparsedArgsNumber = args;
        this.unparsedArgsMessage = msg;
    }

    /**
     * @brief Add extra comments to print as help.
     * @param {...string} messages The additional messages.
     */
    addExtraComments(...messages: Array<string>): void
    {
        const self = this;
        Array.prototype.push.apply(self.extraHelp, messages);
    }

    /**
     * @brief Get the option values.
     */
    getOptionValues(name: string): Array<string>
    {
        const self = this;
        const optData = self.flagsMap.get(name);
        if (typeof optData === "undefined") return [];
        return optData.getValues();
    }

    /**
     * @brief Get the option count.
     */
    getOptionCount(name: string): number
    {
        const self = this;
        const optData = self.flagsMap.get(name);
        if (typeof optData === "undefined") return 0;
        const count = optData.getCount();
        return count;
    }

    /**
     * @brief Get the verbosity level.
     * @return {MyLogLevel | null} The level.
     */
    getVerboseLevel(): MyLogLevel | null
    {
        const self = this;
        if (!self.hasVerbose) return null;
        const count = self.getOptionCount("-v");
        switch (count)
        {
            case 0:
                return MyLogLevel.LEVEL_1;
            case 1:
                return MyLogLevel.LEVEL_2;
            case 2:
                return MyLogLevel.LEVEL_3;
            default:
                return MyLogLevel.LEVEL_3;
        }
    }

    /**
     * @brief Get the list of configurations.
     * @return {Array<string>} The list of configurations.
     */
    getConfigurations(): Array<string>
    {
        const self = this;
        if (!self.hasConfig) return [];
        return self.getOptionValues("-c");
    }

    /**
     * @brief Get the output.
     * @return {string} The output.
     */
    getOutput(): string
    {
        const self = this;
        if (!self.hasOutput) return "";
        return self.getOptionValues("-o")[0];
    }

    /**
     * @brief Get the unparsed args.
     * @return {Array<string>} The list of unparsed args.
     */
    getUnparsedArgs(): Array<string>
    {
        const self = this;
        if (self.unparsedArgsNumber === 0) return [];
        return self.unparsedArgs;
    }

    /**
     * @brief Parse the cli.
     * @return {Promise<void>} A completion promise.
     */
    parse(): Promise<void>
    {
        const self = this;
        self.main.printInfo("Parsing the command line...");

        function doCliParsing(): Promise<void>
        {
            return self._doCliParsing();
        }

        function doCliRefine(): Promise<void>
        {
            return self.doCliRefine();
        }

        function onParsingCompleted(): Promise<void>
        {
            return self.onParsingCompleted();
        }

        function addHelp(): Promise<void>
        {
            return self.addHelp();
        }

        return self.addVersion()
            .then(addHelp)
            .then(doCliParsing)
            .then(doCliRefine)
            .then(onParsingCompleted);
    }

    /**
     * @brief Perform the parsing.
     * @private
     * @return {Promise<void>} A completion promise.
     */
    private _doCliParsing(): Promise<void>
    {
        const self = this;

        const args = process.argv.slice(2);
        for (let index = 0; index < args.length; ++index)
        {
            const arg = args[index];
            if (arg.startsWith("-"))
            {
                const optData = self.flagsMap.get(arg);
                if (typeof optData === "undefined")
                {
                    return Promise.reject(
                        new Error("Unknown flag " + arg)
                    );
                }
                const check = optData.parse(args, index);
                if (check instanceof Error) return Promise.reject(check);
                index = check;
            }
            else
            {
                self.unparsedArgs.push(arg);
            }
        }

        if (self.hasHelp)
        {
            const help = self.flagsMap.get("-h");
            if (typeof help !== "undefined" && help.getCount() > 0)
            {
                return self._printHelp();
            }
        }

        if (self.hasVersion)
        {
            const version = self.flagsMap.get("-V");
            if (typeof version !== "undefined" && version.getCount() > 0)
            {
                return self._printVersion();
            }
        }

        if (self.unparsedArgsNumber === 0 && self.unparsedArgs.length !== 0)
        {
            return Promise.reject(
                new Error("Unknown arguments: " + self.unparsedArgs.join(", "))
            );
        }
        else if (self.unparsedArgsNumber > 0
            && self.unparsedArgs.length !== self.unparsedArgsNumber)
        {
            return Promise.reject(
                new Error("Expected exactly "
                    + self.unparsedArgsNumber + " extra arguments, "
                    + "but found " + self.unparsedArgs.length + ": "
                    + self.unparsedArgs.join(", "))
            );
        }

        for (const flag of self.flags.values())
        {
            if (!flag.isMandatory()) continue;
            const count = flag.getCount();
            if (count !== 0) continue;
            return Promise.reject(
                new Error(
                    "Missing mandatory flag: " + flag.getName()
                )
            );
        }

        return Promise.resolve();
    }

    /**
     * @brief Print the version number and exit.
     * @return {Promise<void>} A completion promise.
     */
    protected _printVersion(): Promise<void>
    {
        const self = this;
        if (self.main.log.getLoggersCount() === 0)
        {
            self.main.log.addLogger(new MyConsoleLogger());
        }
        self.main.log.log(self.main.packageJson.version);
        return self.main.doExit();
    }

    /**
     * @brief Print the help and exit.
     * @return {Promise<void>} A completion promise.
     */
    protected _printHelp(): Promise<void>
    {
        const self = this;
        if (self.main.log.getLoggersCount() === 0)
        {
            self.main.log.addLogger(new MyConsoleLogger());
        }

        function comparator(o1: CliOptionData, o2: CliOptionData): number
        {
            return o1.getName().localeCompare(o2.getName());
        }
        self.flags.sort(comparator);

        // eslint-disable-next-line no-nested-ternary
        const unparsed = self.unparsedArgsNumber === 0
            ? ""
            : self.unparsedArgsNumber > 0
                ? " args[" + self.unparsedArgsNumber + "]"
                : " args...";

        self.main.log.log("");
        self.main.log.info(
            path.basename(process.argv[1])
            + " [options]"
            + unparsed
        );

        self.main.log.log("");
        for (const opt of self.flags.values())
        {
            self.main.log.log(opt.toString());
        }
        if (self.unparsedArgsMessage !== "")
        {
            const argsHelp = self.unparsedArgsNumber > 0
                ? "args[" + self.unparsedArgsNumber + "]"
                : "[args...]";
            self.main.log.log(argsHelp + ": " + self.unparsedArgsMessage);
        }
        for (const msg of self.extraHelp.values())
        {
            self.main.log.log(msg);
        }

        self.main.log.log("");
        return self.main.doExit();
    }

    /**
     * @brief Perform the parsing refinement.
     * @protected
     * @return {Promise<void>} A completion promise.
     */
    protected doCliRefine(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * @brief Do parsing final cleanup.
     * @protected
     * @return {Promise<void>} A completion promise.
     */
    protected onParsingCompleted(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }
}


export {
    Cli
};
