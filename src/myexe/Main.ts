import type {
    Cli,
    Config,
    ICommand
} from "./index";
import {
    MyConsoleLogger,
    MyLogger,
    MyLogLevel
} from "@open-kappa/mylog";
import type {MyJson} from "@open-kappa/myjson";
import type {PackageJson} from "../custom_ts_d/PackageJson";
import path from "path";


/**
 * @brief The main entry point class.
 * @typeParam JsonConfig The JSON configuration type.
 */
class Main<JsonConfig>
{
    /**
     * @brief The log.
     */
    log: MyLogger;

    /**
     * @brief The reference to package.json.
     */
    packageJson: PackageJson;

    /**
     * @brief The cli.
     */
    cli: Cli<JsonConfig>;

    /**
     * @brief The config.
     */
    config: Config<JsonConfig>;

    /**
     * @brief The command to execute.
     */
    command: ICommand;

    /**
     * Whether this lib classes are enabled.
     * @private
     */
    private loggingEnabled: boolean

    /**
     * @brief Constructor.
     * @param {new() => MyLogger} LogClass The logger class.
     * @param {new(main: Main<JsonConfig>) => Cli<JsonConfig>} CliClass The cli
     *     class.
     * @param {new(
     *       main: Main,
     *       jsonValidator: MyJson|((j: any)=> Promise<any>)|null) => MyLogger
     *   } LogClass The logger class.
     * @param {new(main: Main<JsonConfig>) => ICommand} CommandClass The command
     *     to execute.
     * @param {MyJson|((j: any)=> Promise<any>)|null} jsonValidator The JSON
     *     validator
     */
    constructor(
        LogClass: new() => MyLogger,
        CliClass: new(main: Main<JsonConfig>) => Cli<JsonConfig>,
        ConfigClass: new(
            main: Main<JsonConfig>,
            jsonValidator: MyJson|((j: any)=> Promise<any>)|null
        ) => Config<JsonConfig>,
        CommandClass: new(main: Main<JsonConfig>) => ICommand,
        jsonValidator: MyJson|((j: any)=> Promise<any>)|null
    )
    {
        const fake: any = {};

        this.log = new LogClass();
        this.packageJson = fake;
        this.cli = new CliClass(this);
        this.config = new ConfigClass(this, jsonValidator);
        this.command = new CommandClass(this);
        this.loggingEnabled = false;
    }

    /**
     * @brief Enables logging in this library classes.
     * @param {boolean} enable The enabling value.
     */
    enableLogging(enable: boolean): void
    {
        this.loggingEnabled = enable;
    }

    /**
     * @brief Check whether logging is enabledin this library classes.
     * @return {boolean} The check result.
     */
    isLoggingEnabled(): boolean
    {
        return this.loggingEnabled;
    }

    /**
     * @brief Do log for these classes.
     * @param {string} msg The message.
     */
    printLog(msg: string): void
    {
        const self = this;
        if (!self.isLoggingEnabled()) return;
        self.log.log(msg);
    }

    /**
     * @brief Do log for these classes.
     * @param {string} msg The message.
     */
    printInfo(msg: string): void
    {
        const self = this;
        if (!self.isLoggingEnabled()) return;
        self.log.info(msg);
    }

    /**
     * @brief Do log for these classes.
     * @param {string} msg The message.
     */
    printWarn(msg: string): void
    {
        const self = this;
        if (!self.isLoggingEnabled()) return;
        self.log.warn(msg);
    }

    /**
     * @brief Do log for these classes.
     * @param {string} msg The message.
     */
    printError(msg: string): void
    {
        const self = this;
        if (!self.isLoggingEnabled()) return;
        self.log.error(msg);
    }

    /**
     * @brief Do log for these classes.
     * @param {string} msg The message.
     */
    printTrace(msg: string): void
    {
        const self = this;
        if (!self.isLoggingEnabled()) return;
        self.log.trace(msg);
    }

    /**
     * @brief Do log for these classes.
     * @param {string} msg The message.
     */
    printDebug(msg: string): void
    {
        const self = this;
        if (!self.isLoggingEnabled()) return;
        self.log.debug(msg);
    }

    /**
     * @brief Run the program.
     * The returned promise is always successful, but in case of error, the
     * `process.exitCode` is set to 1 and the error information is printed to
     * the log.
     * The internal steps can return a rejected promise, with an error without
     * message, to ask for an immediate successful exit. This is useful for
     * example, to quit with success after parsing the "--help" flag.
     */
    run(): Promise<void>
    {
        const self = this;
        function readPackageJson(): Promise<void>
        {
            return self.readPackageJson();
        }
        function parseCli(): Promise<void>
        {
            return self._parseCli();
        }
        function readConfig(): Promise<void>
        {
            return self._readConfig();
        }
        function setVerbosity(): Promise<void>
        {
            return self.setVerbosity();
        }
        function execCommand(): Promise<void>
        {
            return self._execCommand();
        }
        function onError(err: Error): Promise<void>
        {
            return self._onError(err);
        }

        return readPackageJson()
            .then(parseCli)
            .then(readConfig)
            .then(setVerbosity)
            .then(execCommand)
            .catch(onError);
    }

    /**
     * @brief Read the package.json
     * @protected
     * @return {Promise<void>} A promise completion.
     */
    protected readPackageJson(): Promise<void>
    {
        const self = this;
        self.printInfo("Reading package.json...");
        try
        {
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            const pkgJson = require(
                path.join(__dirname, "..", "..", "package.json")
            );
            self.packageJson = pkgJson;
        }
        catch (err)
        {
            return Promise.reject(err);
        }
        return Promise.resolve();
    }

    /**
     * @brief Parse the command line arguments.
     * @private
     * @return {Promise<void>} A promise completion.
     */
    private _parseCli(): Promise<void>
    {
        const self = this;
        return self.cli.parse();
    }

    /**
     * @brief Read the config.
     * @private
     * @return {Promise<void>} A promise completion.
     */
    private _readConfig(): Promise<void>
    {
        const self = this;
        return self.config.parse();
    }

    /**
     * @brief Set verbosity.
     * @protected
     * @return {Promise<void>} A promise completion.
     */
    protected setVerbosity(): Promise<void>
    {
        const self = this;
        const level = self._getVerboseLevel();
        if (level === null) return Promise.resolve();
        self.log.setVerbosity(level);
        return Promise.resolve();
    }

    /**
     * @brief Execute the command.
     * @private
     * @return {Promise<void>} A promise completion.
     */
    private _execCommand(): Promise<void>
    {
        const self = this;
        return self.command.run();
    }

    /**
     * @brief Manage the errors.
     * @private
     * @param {Error} err The error.
     * @return {Promise<void>} A promise completion.
     */
    private _onError(err: Error): Promise<void>
    {
        const self = this;
        if (self.log.getLoggersCount() === 0)
        {
            self.log.addLogger(new MyConsoleLogger());
        }

        if (err.message === "")
        {
            return Promise.resolve();
        }
        else if (
            self._getVerboseLevel() >= MyLogLevel.LEVEL_2
            && typeof err.stack !== "undefined"
        )
        {
            this.log.error(err.stack);
        }
        else
        {
            this.log.error(String(err));
        }
        process.exitCode = 1;
        return Promise.resolve();
    }

    /**
     * @brief Gets the current verbose level.
     * @private
     * @return {MyLogLevel} The level.
     */
    private _getVerboseLevel(): MyLogLevel
    {
        const self = this;
        const cliVerbose = self.cli.getVerboseLevel();
        if (cliVerbose !== null) return cliVerbose;
        const configVerbose = self.config.getVerboseLevel();
        if (configVerbose !== null) return configVerbose;
        return MyLogLevel.LEVEL_1;
    }

    /**
     * Do the exit promise.
     */
    doExit(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.reject(new Error());
    }
}


export {
    Main
};
