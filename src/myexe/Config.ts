/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
    foreachPromise,
    listFiles
} from "@open-kappa/mypromise";
import {
    JsonMergePolicy,
    mergeJson,
    MyJson,
    parseJsonString
} from "@open-kappa/myjson";
import fs from "fs";
import type {Main} from "./index";
import {MyLogLevel} from "@open-kappa/mylog";


/**
 * @brief The class implementing the configuration parser.
 * It can be extended for customization.
 * @typeParam JsonConfig The JSON configuration type.
 */
class Config<JsonConfig>
{
    /**
     * @brief The reference to main.
     * @protected
     */
    protected readonly main: Main<JsonConfig>;

    /**
     * @brief Reference to the JSON validator.
     */
    readonly jsonValidator: MyJson|((j: any)=> Promise<any>)|null;

    /**
     * @brief The configuration JSON.
     */
    json: JsonConfig;

    /**
     * @brief Constructor.
     * @param {Main} main The reference to main.
     * @param {MyJson|((j: any)=> Promise<any>)|null} jsonValidator The json
     *     validator.
     */
    constructor(
        main: Main<JsonConfig>,
        jsonValidator: MyJson|((j: any)=> Promise<any>)|null
    )
    {
        this.main = main;
        this.jsonValidator = jsonValidator;
        const anyConf: any = {};
        this.json = anyConf;
    }

    /**
     * @brief Parse the configuration.
     */
    parse(): Promise<void>
    {
        const self = this;
        self.main.printInfo("Reading the configuration...");
        const configs = self.getConfigurations();

        function doFinalRefinements(json: any): Promise<void>
        {
            self.json = json;
            return self.doFinalRefinements(json);
        }

        function mergeAllJsons(jsons: Array<any>): Promise<any>
        {
            return mergeJson(jsons, JsonMergePolicy.KEEP_LEFT);
        }

        function validateSingleJson(json: any): Promise<any>
        {
            return self.validateJson(json);
        }
        const validateJsons = foreachPromise(validateSingleJson);

        function mergeJsonDirFoo(jsons: Array<any>): Promise<any>
        {
            return mergeJson(jsons, JsonMergePolicy.CONFLICT);
        }
        const mergeDirJsons = foreachPromise(mergeJsonDirFoo);

        function parseJsons(
            jsons: Array<Array<string>>
        ): Promise<Array<Array<any>>>
        {
            const ret: Array<Array<any>> = [];
            try
            {
                for (const jj of jsons.values())
                {
                    if (jj.length === 0) continue;
                    const kkRet: Array<any> = [];
                    ret.push(kkRet);
                    for (const kk of jj.values())
                    {
                        const parsed = parseJsonString(kk);
                        kkRet.push(parsed);
                    }
                }
            }
            catch (err)
            {
                return Promise.reject(err);
            }
            return Promise.resolve(ret);
        }
        const listFilesFoo = listFiles(".json", true);
        const getAllFiles = foreachPromise(listFilesFoo);
        function readSingleFile(file: string): Promise<string>
        {
            function toString(what: any): Promise<string>
            {
                const ret: string = what;
                return Promise.resolve(ret);
            }
            return fs.promises.readFile(file, {"encoding": "utf8"})
                .then(toString);
        }
        const readFileGroup = foreachPromise(readSingleFile);
        const readAllFiles = foreachPromise(readFileGroup);
        return getAllFiles(configs)
            .then(readAllFiles)
            .then(parseJsons)
            .then(mergeDirJsons)
            .then(validateJsons)
            .then(mergeAllJsons)
            .then(doFinalRefinements);
    }

    /**
     * @brief Get the list of configuration paths.
     * By default, they are taken from the CLI.
     * @protected
     * @return {Array<string>} The list of input configuration paths.
     */
    protected getConfigurations(): Array<string>
    {
        const self = this;
        return self.main.cli.getConfigurations();
    }

    /**
     * @brief Validate a single JSON configuration.
     * Default implamentation calls the internal json validator.
     * @protected
     * @param {any} json The JSON to validate.
     * @return {Promise<any>} The validated JSON.
     */
    protected validateJson(json: any): Promise<any>
    {
        const self = this;
        if (self.jsonValidator === null)
        {
            return Promise.resolve(json);
        }
        else if (self.jsonValidator instanceof MyJson)
        {
            try
            {
                const instance = self.jsonValidator.clone();
                instance.parseJson(json);
                return Promise.resolve(json);
            }
            catch (err)
            {
                return Promise.reject(err);
            }
        }
        else
        {
            return self.jsonValidator(json);
        }
    }

    /**
     * @brief Perform final refinements, after the final parsing.
     * Default implementation does nothing.
     * @protected
     * @param {any} _json The final JSON.
     * @return {Promise<void>} A completion promise.
     */
    protected doFinalRefinements(_json: any): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * @brief Get the verbosity level.
     * This default implementation returns always `null`.
     * @protected
     * @return {MyLogLevel | null} The level.
     */
    getVerboseLevel(): MyLogLevel | null
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return null;
    }
}

export {
    Config
};
