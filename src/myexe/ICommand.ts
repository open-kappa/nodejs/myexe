/**
 * @brief Interface for commands.
 */
export interface ICommand
{

    /**
     * @brief Execute the command.
     * @return {Promise<void>} A completion promise.
     */
    run(): Promise<void>;
}
