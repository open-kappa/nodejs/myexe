This simple package provides an API to write simple utility programs.

The main features are:
1. It is promise based.
1. It is easy to extend and adapt to user needs.
