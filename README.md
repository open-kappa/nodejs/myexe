# @open-kappa/myexe
This package provides a simple library to simplify writing of utility programs.

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/myexe)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/myexe)

 * List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/myexe* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
